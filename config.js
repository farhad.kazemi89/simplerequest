const serverConfig = {    
    hostname : process.env.HOST || 'localhost',
    port : process.env.PORT || 65432,
    eventEmitter : null
  };
  
  const routerConfig = {
    eventEmitter : null
  };
  
  const schemaConfig = {
    eventEmitter : null,
    schemas : {
      signup : '../../schemas/signup.schema.js',
    }
  };
  
  
  const servicesDirectory = './services';
  
  module.exports = {
    serverConfig,
    routerConfig,
    schemaConfig,
    servicesDirectory
  };