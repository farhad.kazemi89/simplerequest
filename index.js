require('dotenv').config({path: './.env'});
const fs = require('fs');
const EventEmitter = require('events');
const Server = require('farhad-initial-server');
const Router = require('farhad-simple-router');
const SchemaCompiler = require('farhad-schema-compiler');
const setDataValidator = require('farhad-data-validator').setCompiledSchema;
const c = require('./config');

const eventEmitter = new EventEmitter();
c.serverConfig.eventEmitter = eventEmitter;
c.routerConfig.eventEmitter = eventEmitter;
c.schemaConfig.eventEmitter = eventEmitter;

const server = new Server(c.serverConfig);
const router = new Router(c.routerConfig);
const schemaCompiler = new SchemaCompiler(c.schemaConfig);

server.start();
eventEmitter.emit('start');
setDataValidator(schemaCompiler.ajvCompile);

// eventEmitter.on('compiledSchemas',()=>{
//   setDataValidator(schemaCompiler.ajvCompile);
// });

loadApps();

/**
* Read the routes and middlewares from route files of each service 
* and load them inside router function 
*
 */
function loadApps(){
  const serviceNames = fs.readdirSync(c.servicesDirectory);
  for (const service of serviceNames){
    // eslint-disable-next-line global-require
    const app = require(`${c.servicesDirectory}/${service}`);
    for (const route in app.routes){
      for (const method in app.routes[route]) {
        const routeObj = {
          route,
          method,
          function: app.routes[route][method].function,
          middlewares: app.routes[route][method].middlewares
        };
        router.addRoute(routeObj);
      }
    }
  }
}