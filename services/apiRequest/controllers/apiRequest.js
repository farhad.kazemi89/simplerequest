// https://stackoverflow.com/questions/6158933/how-is-an-http-post-request-made-in-node-js
const http = require('http');
// const dataParser = require('farhad-data-parser');

class ApiRequest {

    signup(req, res){
        const host = 'localhost';
        const port = '81';

        let reqData = JSON.parse(JSON.stringify(req.data));        

        // Build the post string from an object
        let post_data = JSON.stringify({
        // let post_data = {
            'id' : reqData.nationalCode,
            'parent' : reqData.parent,
            'data' : {
                'userName' : reqData.userName,
                'password' : reqData.password,
                'jobSkill' : reqData.jobSkill,
                'jobTitle' : reqData.jobTitle,
                'fullName' : reqData.fullName,
                'sex' : reqData.sex,
                'education' : reqData.education,
                'email' : reqData.email
            }
        });

        // An object of options to indicate where to post to
        let post_options = {
            host,
            port,
            path: '/dataService',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(post_data)
            }
        };

        // Set up the request
        let post_req = http.request(post_options, res => {
            res.setEncoding('utf8');
            let resData = '';
            res.on('data', chunk => {
                resData += chunk;
            });
            res.on('end', () => {
                console.log('post data result: ',resData);
            });
        });

        // post the data
        post_req.write(post_data);
        post_req.end();        
    }
}

module.exports = {
    ApiRequest
};
