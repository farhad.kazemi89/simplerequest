const ApiRequest = require('./controllers/apiRequest').ApiRequest;
const dataParser = require('farhad-data-parser');
const DataValidator = require('farhad-data-validator').DataValidatorMiddleware;


let apiRequest = new ApiRequest();

module.exports = {
  '/signup': {
    POST: {
      function: (req,res) => {
        return apiRequest.signup(req,res);
      },
      middlewares: [dataParser, new DataValidator('signup')]
    }
  },  
};