const signupSchema = {
  type: "object",
  properties: {
    username: {
      description: "username",
      type: "string",
      minLength: 3,
      isNotEmpty: 1,
    },
    password: {
      description: "Password of the user",
      type: "string",
      //Minimum six and maximum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.
      pattern:
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,8}$",
    },
    nationalCode: {
      description: "key in our redis DBs 0,1 and id of the user",
      type: "string",
      pattern: 
        "^\\d{10}|0$",
    },
    jobSkill: {
      description: "job required skills",
      type: "string",
      minLength: 3,
      isNotEmpty: 1,
    },
    jobTitle: {
      description: "title of the job",
      type: "string",
      minLength: 3,
      isNotEmpty: 1,
    },
    fullName: {
      description: "first name and last name of the user",
      type: "string",
      minLength: 6,
    },
    sex: {
      description: "sex",
      type: "string",
      enum: ["male", "female"],
    },
    education: {
      description: "educational degree type",
      type: "string",
      minLength: 3,
      isNotEmpty: 3,
    },
    email: {
      description: "email of the user",
      type: "string",
      //all the characters permitted by RFC 5322. restrict leading, trailing, or consecutive dots in emails. restrict no. of characters in top level domain.
      pattern:
        "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$",        
    },
    parent: {
      description: "value in our redis DB 1 and parent id of the user",      
      type: "string",
      pattern: 
        "^\\d{10}|0$"
    },
  },

  required: ["nationalCode", "parent"],
  additionalProperties: false,
};

module.exports = signupSchema;

//   "userName" : "Kazemisan",
//   "password" : "Answer$1",
//   "nationalCode" : 0910234568,
//   "jobSkill" : "node developer",
//   "jobTitle" : "backend developer",
//   "fullName" : "Farhad Kazemi",
//   "sex" : "male",
//   "education" : "M.Sc.",
//   "email" : "user@domain.com",
//   "parentId" : 0910234234
